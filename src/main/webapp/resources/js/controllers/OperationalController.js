angular.module('WebApp').controller('OperationalController' , ['$state', '$stateParams','$rootScope', '$scope', 'settings','$filter','Schedule','RDBMS','ListBucket','ListJob','ListServerCredential','Report','Kafka','$http', function($state , $stateParams, $rootScope, $scope,settings,$filter,Schedule,RDBMS ,ListBucket,ListJob,ListServerCredential,Report,Kafka,$http) {

		$scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();
        
         
        $scope.userId= $rootScope.user_data.userId; 
        $scope.date = [
                       {model : "last - 7 days", value : "7"},
                       {model : "last - 14 days", value : "14"},
                       {model : "last - 21 days", value : "21"},
                       {model : "last - 28 days", value : "28"}
                   ]; 
          
        $scope.selected="21";
        $scope.selected2="21";
        $scope.Showdate = new Date();
        
        $scope.getdata = function(data)
        {
        	$scope.MysqlReportJson= '{"classname": "com.mysql.jdbc.Driver","dburl":"jdbc:mysql://107.180.2.11:","dbport":"3306","dbusername":"cloudhitiadmin","dbpassword":"cloudhitiadmin2017","dbname":"cloudhitidemo","tablename":"order_csv","query":" select HOUR( STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' ) )   , DAYNAME( STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' ) ) , count(id) as total ,  STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' ) as new_order_time FROM `order_csv` where  STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' )  BETWEEN NOW()- INTERVAL '+parseInt(data)+' DAY  AND NOW() - INTERVAL '+ (parseInt(data) - 7) +' DAY group by 1, 2 order by  DAY(  STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' ) ) ASC    "}'; 
        	 
         	console.log($scope.MysqlReportJson)
        	$scope.MysqlReportJson = JSON.parse(  $scope.MysqlReportJson );
        	Report.MysqlReport2($scope.MysqlReportJson).then(function (data) {
        		 
    		var allrows= data.allcol;
    		let arrayreturn = [];
    		var localdumpAggregator=[];
    		let arrayreturn2=[null,null,null,null,null,null,null,null,null,null,null,null,null,null,0,0,0,0,0,0,0,0,0,0];
    		let i=0;
    		allrows.forEach(function(valuemain,index )
    			{
    				if( index == 0) { localdumpAggregator.push(valuemain[1]); }
    				
    				if( localdumpAggregator.indexOf(valuemain[1])  != "-1" )
    				{ 
    					 arrayreturn2[valuemain[0]-1]=parseInt (valuemain[2] );
     				}	
    				else
    				{
    					localdumpAggregator.push( valuemain[1] );
    					
    					arrayreturn.push({ 
    				   		"name" : localdumpAggregator[i]  ,  
    						"data":  arrayreturn2
    					});
    					 
    					arrayreturn2=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];;
    					arrayreturn2[valuemain[0]-1]=parseInt (valuemain[2] );
    					i++;
    					 
    				}
    				
    				if( index == allrows.length -1 ) { 
     					arrayreturn.push({ 
 				   		"name" : localdumpAggregator[i]  ,  
 						"data":  arrayreturn2
 					}); 
    				}
    				
    			});
     			 
    		$scope.firstgraph(arrayreturn); 
    		 
     	},function (errorMessage) { 
    		//$scope.attrvalue=0;//$state.go('dashboard', {},{reload: true});
	      });  
        	
        	 
        	//$scope.MysqlReportJson= '{"classname": "com.mysql.jdbc.Driver","dburl":"jdbc:mysql://107.180.2.11:","dbport":"3306","dbusername":"cloudhitiadmin","dbpassword":"cloudhitiadmin2017","dbname":"cloudhitidemo","tablename":"order_csv","query":"  s new_order_time FROM `order_csv` where  STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' )  BETWEEN NOW()- INTERVAL '+parseInt(data)+' DAY  AND NOW() - INTERVAL '+ (parseInt(data) - 7) +' DAY group by 1, 2 order by  DAY(  STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' ) ) ASC    "}'; 

        	$scope.MysqlReportJson= '{"classname": "com.mysql.jdbc.Driver","dburl":"jdbc:mysql://107.180.2.11:","dbport":"3306","dbusername":"cloudhitiadmin","dbpassword":"cloudhitiadmin2017","dbname":"cloudhitidemo","tablename":"order_csv","query":"select `Order Date/Time`, `Aggregator`, `Total Order` FROM `order_csv` where     STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' )  >  NOW() - INTERVAL  7 DAY order by   STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' )  ASC"}';
          	 $scope.MysqlReportJson = JSON.parse(  $scope.MysqlReportJson );
         	 
     	      Report.MysqlReport2($scope.MysqlReportJson).then(function (data) {
    		var totalonline=["ChowNow","UberEats","Eat24","GrubHub","DoorDash"];
    		var allrows= data.allcol;
    		 let arrayreturn = [];
    		totalonline.forEach(function(valuemain,index )
    				{
    		 			
     						let arrayreturn2=[];
     						allrows.forEach(function(value)
    						{  
     							if( value[1] != valuemain)
     							{
     								return false;
     							}
     							var amount= value[2];
     							 
    		 					amount= amount.replace(/\$/g, '');
    							 
       							var now = new Date(value[0] +' UTC');
    							var mydate =   Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
     							arrayreturn2.push( [mydate,parseInt(amount)]  ); 
     						});
    		 				
    					 	arrayreturn.push({ 
    				   		"name" : valuemain  ,  
    						"data":  arrayreturn2
    						}); 
    					 	
    					 	if(  index  ==   totalonline.length-1)
    		 		     	{
    					 		//console.log(arrayreturn);
    					 		 $scope.thirdgraph(arrayreturn);
    		  		     	} 
    				});	
            		  
	     	},function (errorMessage) { 
	    		//$scope.attrvalue=0;//$state.go('dashboard', {},{reload: true});
	          }); 
            
               
        	
        };
        
        $scope.getdata2 = function( data)
        {
      
    	$scope.MysqlReportJson2= '{"classname": "com.mysql.jdbc.Driver","dburl":"jdbc:mysql://107.180.2.11:","dbport":"3306","dbusername":"cloudhitiadmin","dbpassword":"cloudhitiadmin2017","dbname":"cloudhitidemo","tablename":"order_csv","query":"  select  Aggregator, DAYNAME(STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' ) ) , count(id) as total     FROM order_csv where STR_TO_DATE( `Order Date/Time`, \'%m/%d/%Y %h:%i %p\' )   BETWEEN NOW()- INTERVAL '+parseInt(data)+' DAY  AND NOW() - INTERVAL '+ (parseInt(data) - 7) +' DAY   group by 1 , 2 order by  Aggregator asc  "}'; 
     	$scope.MysqlReportJson2 = JSON.parse(  $scope.MysqlReportJson2 );
    	
    	 Report.MysqlReport2($scope.MysqlReportJson2).then(function (data) {
     		var allrows= data.allcol;
     		let arrayreturn = [];
     		var localdumpAggregator=[];
     		var arrayreturn2=[0,0,0,0,0,0,0];
     		let i=0;
     		allrows.forEach(function(valuemain,index )
     			{
     				if( index == 0) { localdumpAggregator.push(valuemain[0]); }
     				
     				if( localdumpAggregator.indexOf(valuemain[0])  != "-1" )
     				{
     				 
     					if(valuemain[1]  =="Monday")
     					{ 
     						arrayreturn2[0]=parseInt (valuemain[2] );
     					}
     					else if(valuemain[1]  =="Tuesday")
     					{
     						arrayreturn2[1]=parseInt (valuemain[2] );
     					}
     					else if(valuemain[1]  =="Wednesday")
     					{
     						arrayreturn2[2]=parseInt (valuemain[2] );
     					}
     					else if(valuemain[1]  =="Thursday")
     					{
     						arrayreturn2[3]=parseInt (valuemain[2] );
     					}
     					else if(valuemain[1]  =="Friday")
     					{
      						arrayreturn2[4]=parseInt (valuemain[2] );
     					}
     					 
     					else if(valuemain[1]  == "Saturday")
     					{
     						arrayreturn2[5]=parseInt (valuemain[2] );
     					}
     					else  
     					{
     						arrayreturn2[6]=parseInt (valuemain[2] );
     					}
     					
     					
     						 
     				}	
     				else
     				{
     					localdumpAggregator.push( valuemain[0] );
     					 
     					arrayreturn.push({ 
     				   		"name" : localdumpAggregator[i]  ,  
     						"data":  arrayreturn2
     					});
     					
     					arrayreturn2=[0,0,0,0,0,0,0];
     					i++;
     					if(valuemain[1]  =="Monday")
     					{ 
     						arrayreturn2[0]=parseInt (valuemain[2] );
     					}
     					else if(valuemain[1]  =="Tuesday")
     					{
     						arrayreturn2[1]=parseInt (valuemain[2] );
     					}
     					else if(valuemain[1]  =="Wednesday")
     					{
     						arrayreturn2[2]=parseInt (valuemain[2] );
     					}
     					else if(valuemain[1]  =="Thursday")
     					{
     						arrayreturn2[3]=parseInt (valuemain[2] );
     					}
     					else if(valuemain[1]  =="Friday")
     					{
     						 
     						arrayreturn2[4]=parseInt (valuemain[2] );
     					}
     					 
     					else if(valuemain[1]  == "Saturday")
     					{
     						arrayreturn2[5]=parseInt (valuemain[2] );
     					}
     					else  
     					{
     						arrayreturn2[6]=parseInt (valuemain[2] );
     					}
     					 
     					
     				}
     				
     				if( index == allrows.length -1 ) { 
     					arrayreturn.push({ 
 				   		"name" : localdumpAggregator[i]  ,  
 						"data":  arrayreturn2
 					}); 
     				
     				}
     			});
     		
     			 
     		 $scope.secondgraph (arrayreturn);
       	  },function (errorMessage) { 
     		//$scope.attrvalue=0;//$state.go('dashboard', {},{reload: true});
 	      }); 
     		
    	$scope.MysqlReportJson2= '{"classname": "com.mysql.jdbc.Driver","dburl":"jdbc:mysql://107.180.2.11:","dbport":"3306","dbusername":"cloudhitiadmin","dbpassword":"cloudhitiadmin2017","dbname":"cloudhitidemo","tablename":"order_csv","query":"  select round(`Distance By Road Miles`) , count(*) total FROM `job_csv`  group by 1  "}'; 
      	$scope.MysqlReportJson2 = JSON.parse(  $scope.MysqlReportJson2 );
     	
     	 Report.MysqlReport2($scope.MysqlReportJson2).then(function (data) {
     		var allrows= data.allcol;
     		var arrayreturn=[]
      		allrows.forEach(function(valuemain,index )
      			{ 	 
        					arrayreturn.push({ 
      				   		"name" : "Distance By Road "+valuemain[0]+" Miles "  ,  
      						"data":  [parseInt (valuemain[1] )]
      					});
      					 console.log(arrayreturn);
       			});
      		
      			 
      		 $scope.fourthgraph (arrayreturn);
        	  },function (errorMessage) { 
      		//$scope.attrvalue=0;//$state.go('dashboard', {},{reload: true});
  	      }); 
      		
    	  
        }
    	
 	    $scope.getdata($scope.selected);
 	    $scope.getdata2($scope.selected);
 	    
 	    
        $scope.firstgraph = function( data)
	        { 
        	Highcharts.chart('firstgraph', {
	            chart: {
	                type: 'line',
	                height: (9 / 16 * 100) + '%' // 16:9 ratio

	            },
	            style: {
	                fontSize: '6px'
	            },
	            title: {
	                text: ''
	            },
	           /* subtitle: {
	                text: 'Source: Order Time'
	            },*/
	            credits:  false,
	                 
	            xAxis: {
	            	title: {
	                    text: 'Time (Hr)',
	                    style: {
	                        fontSize: '6px'
	                    }
	                },
	                labels: {
	                    style: {
	                    	fontSize: '6px'
	                    }
	                },
	                categories: ['01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00','13:00', '14:00','15:00', '16:00','17:00', '18:00','19:00', '20:00','21:00', '22:00','23:00', '24:00']
	            },
	            yAxis: {
	                title: {
	                    text: 'Total Order',
	                    style: {
	                        fontSize: '6px'
	                    }
	                },
	                labels: {
	                    style: {
	                    	fontSize: '4px'
	                    }
	                }
	            },
	             
	            plotOptions: {
	                line: {
	                    dataLabels: {
	                        enabled: true
	                    },
	                    enableMouseTracking: false
	                }
	            }, 
	            series: data
	        });
	        
	    }
       
        $scope.secondgraph = function( data)
        { 

        	Highcharts.chart('secondgraph', {
                chart: {
                    type: 'area',
                    inverted: true,
                    height: (9 / 16 * 100) + '%' // 16:9 ratio
                },
                style: {
	                fontSize: '6px'
	            },
	            title: {
                    text: ''
                },
                credits:  false,
               /* subtitle: {
                    style: {
                        position: 'absolute',
                        right: '0px',
                        bottom: '10px'
                    }
                },*/
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -150,
                    y: 100,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },
                xAxis: {
                	 labels: {
 	                    style: {
 	                    	fontSize: '6px'
 	                    }
 	                },categories: [
                        'Monday',
                        'Tuesday',
                        'Wednesday',
                        'Thursday',
                        'Friday',
                        'Saturday',
                        'Sunday'
                    ]
                },
                yAxis: {
                    title: {
                        text: 'Total Order'
                    },
                    labels: {
                    	 style: {
  	                    	fontSize: '6px'
  	                    },
  	                    formatter: function () {
                            return this.value;
                        }
                    },
                    min: 0
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                series: data
            });
        
        }
        
        $scope.thirdgraph = function( data)
        { 

    		Highcharts.chart('thirdgraph', {
		 	chart: {
		        type: 'spline',
		        height: (9 / 16 * 100) + '%' // 16:9 ratio
		    },
		    title: {
		        text: ''
		    },
		    style: {
                fontSize: '6px'
            },
		    xAxis: {
		        type: 'datetime',
		        //  tickInterval:24 * 3600 * 1000 * 15,
		      //  pointIntervalUnit:10,
		       // pointInterval: 24 * 3600 * 1000 * 30, // one day
		        labels: {
		        	style: {
	                    	fontSize: '6px'
	                    },//format: '{value: %Y-%b-%e %H:%M:%S}'
		        	format: '{value: %Y-%b-%e }'
		          },
		        dateTimeLabelFormats: { // don't display the dummy year
		        	day: '%e. %b',
		        	month: '%b \'%y',
		        	year: '%Y'
		        	},
		        title: {
		            text: 'Date'
		        },
		        
		    },
		    yAxis: {
		        title: {
		            text: 'Total Order($)'
		        },
		        labels: {
		        	style: {
	                    	fontSize: '6px'
	                    } 
 		          },
		         min: 0,
		        
		    },
		    tooltip: {
		        headerFormat: '<b>{series.name}</b><br>',
		        pointFormat: '{point.x:%e. %b}: $ {point.y:.2f}'
		    },

		    plotOptions: {
		        spline: {
			            marker: {
		                enabled: true
		            }
		        }
		    },
		    series:  data
    		});
   
    
        }
        
        $scope.fourthgraph = function( data)
         {
        	Highcharts.chart('fourthgraph', {
         
            chart: {
                type: 'bar',
                height: (9 / 16 * 100) + '%' // 16:9 ratio
            },
            title: {
                text: ''
            },
            
            xAxis: {
                categories: ['Distance' ],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Order',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: 'Total Order'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: data
         });
       
         }
         
        
       
        
        
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });
}]);
 			         
			            